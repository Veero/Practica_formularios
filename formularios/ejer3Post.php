<!DOCTYPE html>
<html>
<head>
	<title>Ejercicio 3 POST</title>
	<link rel="stylesheet" type="text/css" href="EstiloForm.css">
</head>
<body>
<h2>Captura de datos</h2>
<form name="formulario" method="POST" action="POST_procesar.php">
	<table border=1>
		<tr>
			<label for="nombre">Nombre:</label>
	<input type="text" name="nombre" size="50" placeholder="Ingrese su nombre">
		</tr>
		<tr>
			<label for="apellido">Apellido:</label>
	<input type="text" name="apellido" size="50" placeholder="Ingrese su apellido">
		</tr>
		<tr>
			<label for="edad">Edad:</label>
	<input type="text" name="edad" size="50" placeholder="Ingrese su Edad">
		</tr>
		<tr>
			<input type="submit" name="btnEnviar" value="Enviar" class="btn btn-danger">
		</tr>
	</table>
</form>
</body>
</html>